require('dotenv').config();

const bodyParser = require("body-parser");

const express = require("express");
const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const { Pool } = require('pg');

const port = process.env.PORT;
const version = process.env.VERSION;

const pool = new Pool ({
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    database: process.env.DB_DATABASE,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD
});


// https://dzone.com/articles/cors-in-node
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/', (req, res) => {
    res.send(`<h1 style='font-size: 3em;'>REST API v${version}</h1>
    <table style='font-size: 2em;'>
        <tr><td>Host+Port:</td><td style='padding-left: 1em;'><code>${process.env.DB_HOST}:${process.env.DB_PORT}</code></td></tr>
        <tr><td>Database:</td><td style='padding-left: 1em;'><code>${process.env.DB_DATABASE}</code></td></tr>
        <tr><td>User:</td><td style='padding-left: 1em;'><code>${process.env.DB_USER}</code></td></tr>
        <tr><td>get (all)</td><td style='padding-left: 1em;'><code><a href='/getPOI' target='_blank'>/getPOI</a></code></td></tr>
		<tr><td>get (all)</td><td style='padding-left: 1em;'><code><a href='/getLS' target='_blank'>/getLS</a></code></td></tr>
	</table>`);
});

//----------------------------------------------------POI------------------------------------------------------------

app.get('/getPOI', async (req, res, next) => {
    try {
        const query = "SELECT id, cat, name, street, zip, city, tel, poc, info, ST_AsGeoJSON(geom) as geom FROM brausturm_poi";
        let items = [];

        const client = await pool.connect();
        const result = await client.query(query);

        for (let row of result.rows) {
            items.push(feature(row));
        }
        res.json(JSON.parse(geojson(items)));
    } catch(err) {
        next(err);
    }
});

function feature(row) {
    return `{"type": "Feature",
    "properties": {
        "ID": "${row.id}",
        "CAT": "${row.cat}",
        "NAME": "${row.name}",
        "STREET": "${row.street}",
        "ZIP": "${row.zip}",
        "CITY": "${row.city}",
        "TEL": "${row.tel}",
        "POC": "${row.poc}",
        "INFO": "${row.info}"
    },
    "geometry": ${row.geom}
    }`;    
}

function geojson(items) {
    return `{ "type":"FeatureCollection",
    "crs":{  
        "type":"name",
        "properties":{  
            "name":"urn:ogc:def:crs:OGC:1.3:CRS84"
        }
    },
    "features":[
        ${items.join()}
    ] }`;
}

app.post('/createPOI', async (req, res, next) => {
    try {
        const query = `INSERT INTO brausturm_poi
        (id, cat, name, street, zip, city, tel, poc, info, geom)
        VALUES (
            ${req.body.poiId},
            '${req.body.poiCat}',
            '${req.body.poiName}',
            '${req.body.poiStreet}',
            '${req.body.poiZip}',
            '${req.body.poiCity}',
            '${req.body.poiTel}',
            '${req.body.poiPoc}',
            '${req.body.poiInfo}',
            ST_PointFromText('POINT( ${req.body.poiLongitude} 
                                     ${req.body.poiLatitude})', 4326)
        );`;

        const client = await pool.connect();
        const result = await client.query(query);

        res.send("Datensatz wurde hinzugefügt!");
    } catch(err) {
        res.send("Ein Fehler ist aufgetreten!");
        next(err);
    }
});

app.post('/updatePOI', async (req, res, next) => {
    try {
        const query = `UPDATE brausturm_poi
        SET cat='${req.body.poi}',
        name='${req.body.poiName}',
        street='${req.body.poiStreet}',
        zip='${req.body.poiZip}',
        city='${req.body.poiCity}',
        tel='${req.body.poiTel}',
        poc='${req.body.poiPoc}',
        info='${req.body.poiInfo}',
        geom=ST_PointFromText('POINT( ${req.body.poiLongitude} 
                                      ${req.body.poiLatitude})', 4326)
        WHERE id=${req.body.poiId};`;

        const client = await pool.connect();
        const result = await client.query(query);

        res.send("Datensatz geändert!");
    } catch(err) {
        res.send("Ein Fehler ist aufgetreten!");
        next(err);
    }
});

app.post('/deletePOI', async (req, res, next) => {
    try {
        const query = `DELETE FROM brausturm_poi
        WHERE id=${req.body.poiId};`;

        const client = await pool.connect();
        const result = await client.query(query);

        res.send("Datensatz wurde gelöscht!");
    } catch(err) {
        res.send("Ein Fehler ist aufgetreten!");
        next(err);
    }
});

//----------------------------------------------------LS------------------------------------------------------------

app.get('/getLS', async (req, res, next) => {
    try {
        const query = "SELECT id, info, edited, ST_AsGeoJSON(geom) as geom FROM brausturm_ls";
        let items = [];

        const client = await pool.connect();
        const result = await client.query(query);

        for (let row of result.rows) {
            items.push(feature1(row));
        }

        res.json(JSON.parse(geojson(items)));
    } catch(err) {
        next(err);
    }
});

function feature1(row) {
    return `{"type": "Feature",
    "properties": {
        "id": "${row.id}",
        "info": "${row.info}",
        "timestamp": "${row.edited}"
    },
    "id": "${row.id}",
    "geometry": ${row.geom}
    }`;    
}

app.post('/createLS', async (req, res, next) => {
    try {
        const query = `INSERT INTO brausturm_ls
        (id, info, geom)
        VALUES (
            '${req.body.lsId}',
            '${req.body.lsInfo}',
            ST_GeomFromText('${req.body.lsGeometry}', 4326)
        );`;

        console.log(query);

        const client = await pool.connect();
        const result = await client.query(query);

        res.send("Datensatz wurde eingefügt!");
    } catch(err) {
        res.send("Ein Fehler ist aufgetreten!");
        next(err);
    }
});


app.post('/updateLS', async (req, res, next) => {
    try {
        const query = `UPDATE brausturm_ls
        SET info='${req.body.lsInfo}',
        edited=CURRENT_TIMESTAMP,
        geom=ST_GeomFromText('${req.body.lsGeometry}', 4326)
        WHERE id='${req.body.lsId}';`;

        console.log(query);
    
        const client = await pool.connect();
        const result = await client.query(query);

        res.send("Datensatz wurde geändert!");
    } catch(err) {
        res.send("Ein Fehler ist aufgetreten!");
        next(err);
    }
});


app.post('/deleteLS', async (req, res, next) => {
    try {
        const query = `DELETE FROM brausturm_ls WHERE id='${req.body.lsId}';`;

        console.log(query);

        const client = await pool.connect();
        const result = await client.query(query);

        res.send("Datensatz wurde gelöscht!");
    } catch(err) {
        res.send("Ein Fehler ist aufgetreten!");
        next(err);
    }
});

app.listen(port, () => {
    console.log(`
        POI-Server v${version} listening on http://localhost:${port}
        DB_HOST  = ${process.env.DB_HOST}:${process.env.DB_PORT}
        DATABASE = ${process.env.DB_DATABASE}
    `);
});
