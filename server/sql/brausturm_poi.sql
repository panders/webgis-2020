DROP TABLE IF EXISTS brausturm_poi;

CREATE TABLE brausturm_poi (
    id INT NOT NULL PRIMARY KEY,
	cat VARCHAR,
    name VARCHAR,
    street VARCHAR,
    zip VARCHAR(5),
	city VARCHAR,
	tel VARCHAR,
	poc VARCHAR,
	info VARCHAR,
	edited TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    geom geometry(POINT, 4326)
);

INSERT INTO brausturm_poi (id, cat, name, street, zip, city, tel, poc, info, geom) VALUES (20001, 'Kunde', 'EDEKA Center Gillert', 'Wandsbeker Chaussee 248', '22089', 'Hamburg', '+49 40 2099212', 'Herr Fritz', 'Anlieferungszone links in Parkhalle', ST_GeomFromText('POINT(10.058490 53.569328)', 4326));
INSERT INTO brausturm_poi (id, cat, name, street, zip, city, tel, poc, info, geom) VALUES  (20002, 'Kunde','EDEKA Struve', 'Eppendorfer Landstraße 41', '20249', 'Hamburg', '+49 40 481289', 'Frau Muster', 'Anlieferung über Robert-Koch-Straße', ST_GeomFromText('POINT(9.987300 53.591202)', 4326));
INSERT INTO brausturm_poi (id, cat, name, street, zip, city, tel, poc, info, geom) VALUES (30001, 'Lieferant','Landgang Brauerei', 'Beerenweg 12', '22761', 'Hamburg', '+49 40 85158229', NULL, NULL, ST_GeomFromText('POINT(9.922275 53.565021)', 4326));
INSERT INTO brausturm_poi (id, cat, name, street, zip, city, tel, poc, info, geom) VALUES (10002, 'Lager','Brausturm Bierverlag', 'Siebenstücken 8H', '24558', 'Henstedt-Ulzburg', NULL, NULL, NULL, ST_GeomFromText('POINT(9.969579 53.810942)', 4326));
INSERT INTO brausturm_poi (id, cat, name, street, zip, city, tel, poc, info, geom) VALUES (10001, 'Büro','Brausturm Bierverlag', 'Eduardstraße 48', '20257', 'Hamburg', NULL, NULL, NULL, ST_GeomFromText('POINT(9.94833 53.57157)', 4326));
