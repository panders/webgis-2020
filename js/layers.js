import Group from 'ol/layer/Group';
import TileLayer from 'ol/layer/Tile';
import OSM from 'ol/source/OSM';
import TileWMS from 'ol/source/TileWMS';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import {Stroke, Style, Icon} from 'ol/style';
import GeoJSON from 'ol/format/GeoJSON';
import LineString from 'ol/geom/LineString';

const kundenStyle = new Style({
    image: new Icon({
        anchor: [0.5, 46],
        anchorXUnits: 'fraction',
        anchorYUnits: 'pixels',
        src: '/images/kunde48.png',
    }),
});

const bueroStyle = new Style({
    image: new Icon({
        anchor: [0.5, 46],
        anchorXUnits: 'fraction',
        anchorYUnits: 'pixels',
        src: '/images/buero48.png',
    }),
});

const lieferStyle = new Style({
    image: new Icon({
        anchor: [0.5, 46],
        anchorXUnits: 'fraction',
        anchorYUnits: 'pixels',
        src: '/images/lieferant48.png',
    }),
});

const lagerStyle = new Style({
    image: new Icon({
        anchor: [0.5, 46],
        anchorXUnits: 'fraction',
        anchorYUnits: 'pixels',
        src: '/images/lager48.png',
    }),
});

const poi_source = new VectorSource({
    url: 'http://localhost:8081/getPOI',
	format: new GeoJSON()
});

const poi_layer = new VectorLayer({
    title: 'Punktdaten',
    id: 'poi_pg',
    visible: true,
    source: poi_source,
    style: function(feature, resolution) {
        if (feature.get('CAT') === 'Kunde') {
            return kundenStyle;
        } else if (feature.get('CAT') === 'Büro') {
            return bueroStyle;
        } else if (feature.get('CAT') === 'Lieferant') {
            return lieferStyle;
        } else if (feature.get('CAT') === 'Lager') {
            return lagerStyle;
        }
    }
});

export { poi_source, poi_layer };

const lineStyle = function (feature) {
    var geometry = feature.getGeometry();
    var styles = [
        new Style({
            stroke: new Stroke({
                color: '#c70039',
                width: 4
            })
        })
    ];
    geometry.forEachSegment(function(start, end) {
        var dx = end[0] - start[0];
        var dy = end[1] - start[1];
        var m = [(start[0] + end[1])/2, (end[0] + end[1])/2];
        var rotation = Math.atan2(dy, dx);

        var lineStr1 = new LineString([end, [end[0] - 10, end[1] + 10]]);
        lineStr1.rotate(rotation, end);
        var lineStr2 = new LineString([end, [end[0] - 10, end[1] - 10]]);
        lineStr2.rotate(rotation, end);

        var stroke = new Stroke({
            color: '#c70039',
            width: 4
        });

        styles.push(new Style({
            geometry: lineStr1,
            stroke: stroke
        }));
        styles.push(new Style({
            geometry: lineStr2,
            stroke: stroke
        }));
    });
    return styles;
};

const ls_source = new VectorSource({
    url: 'http://localhost:8081/getLS',
	format: new GeoJSON()
});

const ls_layer = new VectorLayer({
    id: 'ls_pg',
    title: 'Linien',
    visible: true,
    source: ls_source,
    style: lineStyle
});

export { ls_source, ls_layer };

const BASELAYER = new Group({
    'title': 'Basiskarten',
    'id': 'baselayer',
    'type': 'base',
    layers: [
        new TileLayer({
            id: 'OSM',
            title: 'OpenStreetMap',
            visible: true,
            source: new OSM()
        }),        
        new TileLayer({
            id: 'OrthoLGV20',
            title: 'DOP20 (LGV)',
            visible: false,
            source: new TileWMS({
                url: 'https://geodienste.hamburg.de/HH_WMS_DOP',
                params: {
                    'LAYERS': '1',
                    'FORMAT': 'image/png'
                },
                attributions: [
                    '&copy; Freie und Hansestadt Hamburg, Landesbetrieb Geoinformation und Vermessung'
                ]
            })
        }),
    ]
});

const SELF = new Group({
    id: '',
    title: 'Erhobene Daten',
    layers: [
        poi_layer,
        ls_layer,
    ]
});

const WETTER = new Group({
    id: 'Wetter',
    title: 'Wetter',
    layers: [
        // https://maps.dwd.de/geoserver/web/
        // https://maps.dwd.de/geoserver/dwd/wms?SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&FORMAT=image%2Fpng&TRANSPARENT=true&STYLES&LAYERS=dwd%3AWarngebiete_Kreise&SRS=EPSG%3A4326&WIDTH=513&HEIGHT=435&BBOX=4.8180466730459806%2C46.3852008683787%2C16.09001932929598%2C55.9433063371287
        new TileLayer({
            id: "DWDRegen",
            title: "DWD Regenradar",
            visible: false,
            opacity: 0.5,
            source: new TileWMS({
                url: 'https://maps.dwd.de/geoserver/dwd/wms?',
                params: {
                    'LAYERS': 'dwd:RX-Produkt',
                    'FORMAT': 'image/png'
                },
                attributions: [
                    '&copy; <a href="http://www.dwd.bund.de">DWD</a>'
                ]
            })
        }),
    ]
});


function createLayerPanel(panel, layergroups) {
  let div = document.getElementById(panel);
  
  let h1 = document.createElement("h1"); /* <h1></h1> */
  h1.innerHTML = "Layer"; /* <h1>Layer</h1> */
  div.appendChild(h1);
    
  for(let layergroup of layergroups) {
    let h2 = document.createElement("h2");
    h2.innerHTML = layergroup.get('title');
    div.appendChild(h2);
  
    let type = "checkbox";
    if (layergroup.get('type') == 'base') {
      type = "radio";
    }

    let layers = layergroup.getLayers();
    let ul = document.createElement("ul");
    layers.forEach( (element, index, array) => {
      let li = document.createElement("li");
  
      let input = document.createElement("input");
      input.type = type
      input.name = layergroup.get('id');
      input.value = element.get('id');
      if (element.get('visible')) {
        input.defaultChecked = true;
      }
      input.addEventListener('change', function() {
        let radio_checkbox = this;
        let layerElementValue = radio_checkbox.value;
        if (layergroup.get('type') == 'base') {
          layergroup.getLayers().forEach( (element, index, array) => {
            element.setVisible(element.get('id') === layerElementValue);
          })
        } else {
          layergroup.getLayers().forEach(function(element, index, array) {
            if (element.get('id') === layerElementValue) {
              element.setVisible(radio_checkbox.checked);
            }
          })          
        }
      })
  
      let label = document.createElement("label");
      label.htmlFor = input.id;
      label.appendChild(input);
      let text = document.createTextNode(element.get('title'));
      label.appendChild(text);
  
      li.appendChild(label);
      ul.appendChild(li);
    })
    div.appendChild(ul);
  }
}


export { createLayerPanel, BASELAYER, SELF, WETTER };