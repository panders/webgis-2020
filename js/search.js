import 'ol-ext/control/Search.css';
import Search from 'ol-ext/control/SearchNominatim';
import { map } from './index';

import $ from 'jquery';
window.jQuery = window.$ = $;

const search = new Search({
    target: $("#search > div").get(0),
    position: true,
    placeholder: 'Suche ...',
    maxItems: 5
    
});

search.clearHistory();

search.on('select', function(e) {
    map.getView().setCenter(e.coordinate)
    map.getView().setZoom(17);
});

export { search };