import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import Feature from 'ol/Feature';
import Point from 'ol/geom/Point';
import {Circle as CircleStyle, Fill, Icon, Stroke, Style} from 'ol/style';
import Polyline from 'ol/format/Polyline';
import { transform } from 'ol/proj';
import { format } from 'ol/coordinate';
import $ from 'jquery';
window.jQuery = window.$ = $;

import { map } from './index';

//Routing
let routeLayer;

function routing_click(event) {
    let coord3857 = event.coordinate;
    let coord4326 = transform(coord3857, 'EPSG:3857', 'EPSG:4326');
    document.getElementById('mouseLon').innerText = format(coord4326, "{x}", 6);
    document.getElementById('mouseLat').innerText = format(coord4326, "{y}", 6);
}

//Wegpunkt hinzufügen
$('#waypoint-add').click( () => {
    let lat = $('#mouseLat').text();
    let lon = $('#mouseLon').text();
    let pos = lon + ", " + lat;
    $('#waypoints').append($('<option>', {
        value: pos,
        text: pos
    }));
});

//Wegpunkt entfernen
$('#waypoint-remove').click(function() {
    let waypoint = $('#waypoints option:selected');
    waypoint.remove();
});

//Wegpunkt mit Doppelklick entfernen
$(document).on('dblclick', '#waypoints option', function() {
    let waypoint = $('#waypoints option:selected');
    waypoint.remove();
});

//Wegpunkt hoch
$('#waypoint-up').click(function(){
	$('#waypoints option:selected:first-child').prop("selected", false);
	let before = $('#waypoints option:selected:first').prev();
	$('#waypoints option:selected').detach().insertBefore(before);
});

//Wegpunkt runter
$('#waypoint-down').click(function(){
	$('#waypoints option:selected:last-child').prop("selected", false);
	let after = $('#waypoints option:selected:last').next();
	$('#waypoints option:selected').detach().insertAfter(after);
});

//Route entfernen
$('#route-remove').click(function(){
	$('#waypoints').empty();
	$('#distance').text("");
	$('#time').text("");
	map.removeLayer(routeLayer);
});

//zur Route zoomen
$('#route-zoom').click(function(){
	let layerExtent = routeLayer.getSource().getExtent();
	map.getView().fit(layerExtent, map.getSize());
});

// Route berechnen
$('#route-get').click(function(){
	//console.log('GET ROUTE');
	map.removeLayer(routeLayer);
	let waypoints = '{"coordinates":[';
	$('#waypoints option').each(function() {
		waypoints += "[" + this.value + "],";
	});
	waypoints = waypoints.substr(0, waypoints.length-1);
	waypoints += "]}";
	
	$.ajax({
		url: 'https://api.openrouteservice.org/v2/directions/driving-car', 
		type: 'POST',
		data: waypoints,
		headers: {
				'Accept': 'application/json, text/plain, */*',
				'Content-Type': 'application/json; charset=utf-8',
				'Authorization': '58d904a497c67e00015b45fc3a041537db9b4f7cbbeb6a4feab7a16a'
		},
		dataType: 'json',
		success: (data) => { 
            let polyline = data.routes[0].geometry;
            let route = (new Polyline({
                factor: 1e5 // WICHTIG!!!
            }).readGeometry(polyline, {
                dataProjection: 'EPSG:4326',
                featureProjection: 'EPSG:3857'
             }));
            //console.log(route);

            let routeCoords = route.getCoordinates();
            let routeLength = routeCoords.length;

            let routeFeature = new Feature({
                type: 'route',
                geometry: route
            });

            let startIcon = new Feature({
                type: 'start',
                geometry: new Point(routeCoords[0])
            });

            let endIcon = new Feature({
                type: 'finish',
                geometry: new Point(routeCoords[routeLength - 1])
            });

            let styles = {
                'route': new Style({
                    stroke: new Stroke({
                        width: 6, color: [0, 0, 0, 0.8]
                    })
                }),
                'start': new Style({
                    image: new Icon({
                        anchor: [0.5, 1],
                        src: 'images/marker48.png'
                    })
                }),
                'finish': new Style({
                    image: new Icon({
                        anchor: [0.5, 1],
                        src: 'images/finish48.png'
                    })
                })
            };

            routeLayer = new VectorLayer({
                visible: true,
                source: new VectorSource({
                    features: [routeFeature, startIcon, endIcon]
                }),
                style: (feature) => {
                    return styles[feature.get('type')];
                }
            });

            map.addLayer(routeLayer);
			
            var distance = Math.round(data.routes[0].summary.distance/100)/10;
            $('#distance').text(distance);
			let ttime = data.routes[0].summary.duration;
			let sek = ttime % 60;
			let min = ((ttime - sek) / 60) % 60;
			let std = (((ttime - sek) / 60) - min) / 60;
			let totalTime = ('00'+std).slice(-2) + ':' + ('00'+min).slice(-2) + ':' + ('00'+sek).slice(-2);
            $('#time').text(totalTime);
		},
		error: (data) => {
			$('#route-info').text(data.responseText.error.message);
        }
	});
});



export { routing_click }