import Feature from 'ol/Feature';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import Overlay from 'ol/Overlay';
import Draw from 'ol/interaction/Draw';
import Modify from 'ol/interaction/Modify';
import {Fill, Stroke, Style} from 'ol/style';
import WKT from 'ol/format/WKT';
import Select from 'ol/interaction/Select';
import {click} from 'ol/events/condition';
import { v4 as uuidv4 } from 'uuid';
import $ from 'jquery';
window.jQuery = window.$ = $;

import { map } from './index';
import { ls_source, ls_layer } from './layers';

var draw, snap, modify;
var lineID;

var lineFeature = new Feature();
var lineSource = new VectorSource();
var lineLayer = new VectorLayer({
	visible: true,
	source: lineSource,
	style: new Style({
        fill: new Fill({
            color: 'rgba(179, 236, 255, 0.6)'
        }),
        stroke: new Stroke({
            color: '#00BFFF',
            width: 1
        })
    })
});

export { lineLayer };

// NIX: das Formular wird versteckt
$('#lsControlNone').click(function() {
	$('#lsControlDraw').attr('disabled', false);
	$('#lsControlModify').attr('disabled', false);
	$('#lsControlDelete').attr('disabled', false);
	//Formular verstecken
	$('#lsForm').hide();

	removeInteractions();

	lineSource.clear();
});

// DRAW: Formular wird angezeigt und lsCreate aufgerufen
$('#lsControlDraw').click(function() {
	// DISABLE andere Menüpunkte
	$('#lsControlModify').attr('disabled', true);
	$('#lsControlDelete').attr('disabled', true);
	// Formular anzeigen
	$('#lsForm').show();
	// Formularinhalte werden gelöscht
	lsClearForm();

	removeInteractions();

	lsCreate();
});

// MODIFY: Formular wird angezeigt und modify eingeschaltet
$('#lsControlModify').click(function() {
	// DISABLE andere Menüpunkte
	$('#lsControlDraw').attr('disabled', true);
	$('#lsControlDelete').attr('disabled', true);
	// Formular anzeigen
	$('#lsForm').show();
	// Formularinhalte werden gelöscht
	lsClearForm();

	removeInteractions();

	lsModify();
});

// DELETE: das wird Formular versteckt und alle Aktionen deaktiviert
$('#lsControlDelete').click(function() {
	// Formular ausschalten
	$('#lsForm').hide();
	
	removeInteractions();
});


// Features werden ermittelt, die sich an dem angklickten Pixel befinden.
// Die Attribute werden als Popup angezeigt.

function ls_click(map, event) {
	// Popup
	lineFeature = map.forEachFeatureAtPixel(event.pixel, function (f, layer) {
		// Dem feature-Objekt wird das Attribut layerTitle hinzugefügt.
		try {
			f.layerId = layer.get("id");
			return f;
		} catch (err) {}
	});

	if (lineFeature) {
		if (lineFeature.layerId == "ls_pg") {
			if ($('#lsControlModify').is(':checked')) {
				lsUpdate(lineFeature);
			} else if ($('#lsControlDelete').is(':checked')) {
				lsDelete(lineFeature);
			} else { // Popup-Fenster
				lsContent.innerHTML = "ID: " + lineFeature.get("id") + "<br>" + lineFeature.get("info");
                lineOverlay.setPosition(event.coordinate);
			}
		}
	}
}

export { ls_click };


// ---------------------------------------------------------------
// Erzeugen einer neuen Laufstrecke
// ---------------------------------------------------------------

function lsCreate() {
	// Zeichnen einschalten
	draw = new Draw({
		source: lineSource,	// Quelle des temporären Layers
		type: 'LineString'
	});
	map.addInteraction(draw);
	
	// nach dem Beenden des Zeichnens
	draw.on('drawend', function(e) {
		// Zeichnen ausschalten
		map.removeInteraction(draw);
		
		// das an die Funktion übergebene gezeichnete Feature
		var f = e.feature;
        // WKT erzeugen
        let formatWKT = new WKT();
		let featureWKT = formatWKT.writeFeature(f, {
			dataProjection: 'EPSG:4326',
			featureProjection: 'EPSG:3857',
			rightHanded: true,
			decimals: 6
		});
		$('#lsGeometry').val(featureWKT);
	}, this);
	
	// das Ändern der Lages des Punktes ermöglichen
	modify = new Modify({
		source: lineSource
	});
	map.addInteraction(modify);

	// nach dem Ändern
	modify.on('modifyend', function(e) {
		// alle Features werden übergeben ...
		let features = e.features;
		// ... daher wird nur das erste Feature extrahiert
		let f = features.item(0);
        // WKT erzeugen
        let formatWKT = new WKT();
        let featureWKT = formatWKT.writeFeature(f, {
            dataProjection: 'EPSG:4326',
            featureProjection: 'EPSG:3857',
			rightHanded: true,
            decimals: 6
        });
        $('#lsGeometry').val(featureWKT);
	});
}

// ---------------------------------------------------------------
// Erzeugen einer neuen Linie
// ---------------------------------------------------------------

function lsModify() {
	modify = new Modify({ 
		source: ls_source 
	});

	lsChangeInteraction();

	// nach dem Ändern
	modify.on('modifyend', function(e) {
		// alle Features werden übergeben ...

		// ... daher muss das selektierte Feature herausgesucht werden
		let feature = undefined;
		for (let f of e.features.getArray()) {
			if (lineID === f.getId()) {
				feature = f;
			}
		}

        // WKT erzeugen
        let formatWKT = new WKT();
        let featureWKT = formatWKT.writeFeature(feature, {
            dataProjection: 'EPSG:4326',
            featureProjection: 'EPSG:3857',
			rightHanded: true,
            decimals: 6
        });
        $('#lsGeometry').val(featureWKT);
	});
}

// ---------------------------------------------------------------
// Wenn im Laufstrecken-Formular auf Speichern geklickt wird.
// ---------------------------------------------------------------

$('#lsForm').submit(function(e) {
	// damit keine neue Seite geladen wird
	e.preventDefault();
	
	// Je nachdem welcher Menüpunkt ausgewählt wurde ...
	var url;
	// ... wird entweder eine neue Laufstrecken hinzugefügt oder ...
	if ( $('#lsControlDraw').is(':checked') ) {
		// Feauture-ID wird gesetzt
		$('#lsId').val(uuidv4());
		url = 'http://localhost:8081/createLS';
	// es werden die geänderten Daten einer bereits bestehenden Laufstrecken gespeichert.
	} else if ( $('#lsControlModify').is(':checked') ) {
		url = 'http://localhost:8081/updateLS';
	}
	
	// Daten per AJAX (Asynchronous JavaScript and XML) übertragen
	let form = $(this).serializeArray();
	let form_data = {};
	$.each(form, (i, v) => {
        form_data[v.name] = v.value;
	});

	$.ajax({
		type: 'POST',
		url: url,
		data: form_data
	}).done(function(response) {
		// Meldung, dass die Daten gespeichert/geändert wurden, werden vom Server geliefert
		$('#lsMessage').html(response);
		// Meldung nach 5s wieder verschwinden lassen
		setTimeout(removeMessage, 5000);
	}).fail(function(response) {
		$('#lsMessage').html(response);
	});

	// Wenn eine neue Laufstrecken erstellt wurde, dann ...
	if ( $('#lsControlDraw').is(':checked') ) {
		// ... wird das Feature dem Laufstrecken-Layer hinzugefügt und ...
		let features = [];
		lineSource.forEachFeature(f => {
			features.push(f);
		});
		let f = features[0];

		// ... die Properties mit den Werten aus dem Formular befüllt.
		f.setProperties({
			'id': $('#lsId').val(),
			'info': $('#lsInfo').val()
		});
		f.setId($('#lsId').val());

		// Das neu erzeugte Laufstrecken-Feature wird dem Laufstrecken-Layer hinzugefügt.
		ls_layer.getSource().addFeature(f);
		// Die Quelle, in der das Laufstrecken-Feature erstellt wurde, wird geleert.
		lineSource.clear();
		
		// Laufstrecken-Formular verstecken
		$('#lsForm').hide();
	}

	// Wenn die Daten einer Laufstrecke geändert wurden, ...
	if ( $('#lsControlModify').is(':checked') ) {
		// ... werden die geänderten Formulardaten in das Feature geschrieben.
		lineFeature.setProperties({
			'info': $('#lsInfo').val()
		});
		// Laufstrecken-Formular verstecken
		$('#lsForm').hide();
	}

	// alle Interaktionen werden deaktiviert
	removeInteractions();
	
	// CRUD-Aktionen auf nix setzen
	$('#lsControlNone').prop('checked', true);

	// DISABLE andere Menüpunkte 
	$('#lsControlDraw').attr('disabled', false);
	$('#lsControlModify').attr('disabled', false);
	$('#lsControlDelete').attr('disabled', false);
});


// ---------------------------------------------------------------
// Änderungen aus dem Feature in das Formular schreiben
// ---------------------------------------------------------------

function lsUpdate(f) {
	let coordinate = f.getGeometry().getCoordinates();
	$('#lsId').val(f.get('id'));
	$('#lsInfo').val(f.get('info'));

	/*modify.on('modifyend', function(e) {
		lsUpdate(f);
	});*/
}

// ---------------------------------------------------------------
// biking löschen
// ---------------------------------------------------------------

function lsDelete(f) {
	lsContent.innerHTML = "<span style='margin-left: 10px;'>ID: " + f.get("id") + "</span></p>" +
						"<p>" + f.get("info") + "</p>" +
						"<p>Soll die Linie gelöscht werden?</p>" +
						"<button type='button' class='btn btn-primary' id='lsDelete'>Löschen</button>&nbsp;" +
						"<button type='button' class='btn btn-primary' id='lsAbbruch'>Abbruch</button>";
    lineOverlay.setPosition(f.getGeometry().getCoordinates()[0]);

	$('#lsDelete').click(function() {
		$.ajax({
			type: 'POST',
			url: 'http://localhost:8081/deleteLS',
			data: "lsId=" + f.get('id')
		}).done(function(response) {
			$('#lsMessage').html(response);
			setTimeout(removeMessage, 5000);
		}).fail(function(response) {
			$('#lsMessage').html(response);
		});
		ls_layer.getSource().removeFeature(f);
		lineOverlay.setPosition(undefined);
		lsCloser.blur();
	});

	$('#lsAbbruch').click(function() {
		lineOverlay.setPosition(undefined);
		lsCloser.blur();
	});
}

// --------------------------------------wiederkehrende Aktionen-------------------------------------------

// Meldung löschen
function removeMessage() {
	$('#lsMessage').empty();
}

// ALLE Interaktionen deaktivieren
function removeInteractions() {
	map.removeInteraction(draw);
	map.removeInteraction(snap);
	map.removeInteraction(modify);
	map.removeInteraction(select);

	map.getInteractions().pop();
}

// Formularinhalte löschen
function lsClearForm() {
	$('#lsId').val('');
	$('#lsInfo').val('');
	$('#lsGeometry').val('');
}

// -----------------------------------PopUp--------------------------------------------------------------

// Zugriff auf die Popup-Elemente herstellen.
var lsContainer = document.getElementById('ls_popup');
var lsContent = document.getElementById('ls_popup-content');
var lsCloser = document.getElementById('ls_popup-closer');

var lineOverlay = new Overlay({
		element: lsContainer,
		autoPan: true,
		autoPanAnimation: {
			duration: 250
		}
});

export { lineOverlay };

lsCloser.onclick = function() {
	lineOverlay.setPosition(undefined);
	lsCloser.blur();
	
	return false;
};

let select = null;

let selectClick = new Select({
	condition: click,
	layers: [ls_layer]
});

// Style der beim Ändern der Linie verwendet wird
let styleSelected = new Style({
    stroke: new Stroke({
		lineDash: [10, 20],
        width: 5,
		color: 'red'
    })
});

let lsChangeInteraction = function () {
	if (select !== null) {
		map.removeInteraction(select);
	}

	select = selectClick;

	if (select !== null) {
		map.addInteraction(select);
		select.on('select', function (e) {
			if (e.selected.length > 0) {
				e.selected[0].setStyle(styleSelected);
				lineID = e.selected[0].getId();
				$('#lsId').val(lineID);
				$('#lsInfo').val(e.selected[0].get('info'));
				let formatWKT = new WKT();
				let featureWKT = formatWKT.writeFeature(e.selected[0], {
					dataProjection: 'EPSG:4326',
					featureProjection: 'EPSG:3857',
					rightHanded: true,
					decimals: 6
				});
				$('#lsGeometry').val(featureWKT);
				map.addInteraction(modify);
			} else {
				map.removeInteraction(null);
			}
		});
	}
};
