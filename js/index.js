import 'ol/ol.css';
import './../css/index.css';
import './../css/popup.css';
import '@fortawesome/fontawesome-free/css/all.css';
import Map from 'ol/Map';
import View from 'ol/View';
import { fromLonLat } from 'ol/proj';
import { map_events } from './map_events';
import { createLayerPanel } from './layers';
import { BASELAYER, SELF, WETTER } from './layers';
import './routing';
import { pointLayer, overlay } from './poi_pg';
import { lineLayer, lineOverlay } from './ls_pg';
import { search } from './search';


let map = new Map({
  target: 'map',
  view: new View({
    center: fromLonLat([10.00, 53.55]),
    zoom: 13
  }),
  layers: [BASELAYER, WETTER, SELF]
});

map.addLayer(pointLayer);
map.addOverlay(overlay);

map.addLayer(lineLayer);
map.addOverlay(lineOverlay);

map.addControl(search);

map.updateSize();

createLayerPanel('baselayer', [BASELAYER, WETTER, SELF]);

map_events(map);

export { map };