import { info_click, info_pointermove, info_moveend, info_change_resolution, zoom_resolution_changed } from './info';
import { poi_click } from './poi_pg';
import { ls_click } from './ls_pg';
import { routing_click } from './routing';

function map_events(map) {
  map.on('pointermove', function(event){
    info_pointermove(event);
  });
    
  map.on('click', (event) => {
    info_click(event);
    poi_click(map, event);
    ls_click(map, event);
    routing_click(event);
  });
    
  map.on('moveend', (event) => {
    info_moveend(map);
  });
    
  map.getView().on('change:resolution', (event) => {
    info_change_resolution(map);
  });
    
  zoom_resolution_changed(map); 
}

export { map_events };