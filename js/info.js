import { transform } from 'ol/proj';
import { format } from 'ol/coordinate';

function info_click(event) {
  let coord3857 = event.coordinate;
  let coord4326 = transform(coord3857, 'EPSG:3857', 'EPSG:4326');
  let lat = format(coord4326, "{y}", 8);
  let lon = format(coord4326, "{x}", 8);
  document.getElementById('mouseClickedAt').innerHTML = lon + ", " + lat;
}

function info_pointermove(event) {
  let coord3857 = event.coordinate;
  let coord4326 = transform(coord3857, 'EPSG:3857', 'EPSG:4326');
  document.getElementById('mouseCoord3857').innerHTML = format(coord3857, "{x}, {y}", 2);
  document.getElementById('mouseCoord4326').innerHTML = format(coord4326, "{x}, {y}", 6);
}

function info_moveend(map) {
  let bbox = map.getView().calculateExtent(map.getSize());
  document.getElementById('bbox').innerHTML = bbox[0].toFixed(2) + ", " +
                                              bbox[1].toFixed(2) + ", " +
                                              bbox[2].toFixed(2) + ", " +
                                              bbox[3].toFixed(2);
  let center = transform(map.getView().getCenter(), 'EPSG:3857', 'EPSG:4326');
  document.getElementById('center').innerHTML = format(center, "{x}, {y}", 6);
}

function info_change_resolution(map) {
  zoom_resolution_changed(map);
}

function zoom_resolution_changed(map) {
  document.getElementById('zoom').innerHTML = map.getView().getZoom();
  document.getElementById('resolution').innerHTML = map.getView().getResolution();
}

export { info_click, info_pointermove, info_moveend, info_change_resolution, zoom_resolution_changed };
