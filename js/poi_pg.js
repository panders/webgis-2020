import Feature from 'ol/Feature';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import Overlay from 'ol/Overlay';
import Draw from 'ol/interaction/Draw';
import Modify from 'ol/interaction/Modify';
import {Style, Fill, Stroke, Circle} from 'ol/style';
import { transform } from 'ol/proj';
import { format } from 'ol/coordinate';

import $, { removeData } from 'jquery';
window.jQuery = window.$ = $;

import { map } from './index';
import { poi_source, poi_layer } from './layers';

var draw, snap, modify;

var fill = new Fill({
	color: '#3399CC'
});

var stroke = new Stroke({
	color: 'white',
	width: 1.25
});

var kundenStyle = new Style({
    image: new Circle({
		fill: fill,
		stroke: stroke,
		radius: 6
	}),
	fill: fill,
	stroke: stroke
});

// Erstellen eines temporären Layers
var feature = new Feature();
var pointSource = new VectorSource();
var pointLayer = new VectorLayer({
	visible: true,
	source: pointSource,
	style: kundenStyle
});

export { pointLayer };


// ---------------------------------------------------------------
// Menü-Funktionen
// ---------------------------------------------------------------

// NIX: das wird Formular versteckt und alle Aktionen deaktiviert
$('#poiControlNone').click(function() {
	// ENABLE andere Menüpunkte 
	$('#poiControlDraw').attr('disabled', false);
	$('#poiControlModify').attr('disabled', false);
	$('#poiControlDelete').attr('disabled', false);
	// Formular ausschalten
	$('#formPOI').hide();

	removeInteractions();

	// Die Quelle, in der das Stadtrad-Feature erstellt wurde, wird geleert.
	pointSource.clear();
});

// DRAW: Formular wird angezeigt und createPOI aufgerufen
$('#poiControlDraw').click(function() {
	// DISABLE andere Menüpunkte
	$('#poiControlModify').attr('disabled', true);
	$('#poiControlDelete').attr('disabled', true);
	// Formular anzeigen
	$('#formPOI').show();
	$('#poiId').attr('readonly', false);
	// Formularinhalte werden gelöscht
	clearForm();

	createPOI();
});

// MODIFY: Formular wird angezeigt und modify eingeschaltet
$('#poiControlModify').click(function() {
	// DISABLE andere Menüpunkte
	$('#poiControlDraw').attr('disabled', true);
	$('#poiControlDelete').attr('disabled', true);
	// Formular anzeigen
	$('#formPOI').show();
	// Stationsnummer darf nicht geändert werden, da Primärschlüssel
	$('#poiId').attr('readonly', true);
	// Formularinhalte werden gelöscht
	clearForm();

	modify = new Modify({source: poi_source});
	map.addInteraction(modify);
});

// DELETE: das wird Formular versteckt und alle Aktionen deaktiviert
$('#poiControlDelete').click(function() {
	// Formular ausschalten
	$('#formPOI').hide();
	
	removeInteractions();
});


// Features werden ermittelt, die sich an dem angklickten Pixel befinden.
// Die Attribute werden als Popup angezeigt.

function poi_click(map, event) {
	// Popup
	feature = map.forEachFeatureAtPixel(event.pixel, function (feature, layer) {
		// Dem feature-Objekt wird das Attribut layerTitle hinzugefügt.
		try {
			feature.layerId = layer.get("id");
			return feature;
		} catch (err) {}
	});

	if (feature) {
		if (feature.layerId == "poi_pg") {
			if ($('#poiControlModify').is(':checked')) {
				updatePOI(feature);
			} else if ($('#poiControlDelete').is(':checked')) {
				deletePOI(feature);
			} else { // Popup-Fenster
				content.innerHTML = 
					"<dl>" +
						"<dt><strong>Kunden-ID:" + "</strong></dt>" +
						"<dd>" + feature.get("ID") + "</dd>" +
						"<dt><strong>Name:" + "</strong></dt>" +
						"<dd>" + feature.get("NAME") + "</dd>" +
						"<dt><strong>Adresse:" + "</strong></dt>" +
						"<dd>" + feature.get("STREET") + "<br>" +
						feature.get("ZIP") + " " + feature.get("CITY") + "</dd>" +
						"<dt><strong>Telefon: " + "</strong></dt>" +
						"<dd>" + feature.get("TEL") + "</dd>" +
						"<dt><strong>Ansprechpartner: " + "</strong></dt>" +
						"<dd>" + feature.get("POC") + "</dd>" +
						"<dt><strong>Info: " + "</strong></dt>" +
						"<dd>" + feature.get("INFO") + "</dd>"
				  	"</dl>"; 
				overlay.setPosition(event.coordinate);
			}
		}
	}
}

export { poi_click };


// ---------------------------------------------------------------
// Erzeugen einer neuen Stadtrad-Station
// ---------------------------------------------------------------

function createPOI() {
	// Zeichnen einschalten
	draw = new Draw({
		source: pointSource,	// Quelle des temporären Layers
		type: 'Point'
	});
	map.addInteraction(draw);
	
	// nach dem Beenden des Zeichnens
	draw.on('drawend', function(e) {
		// Zeichnen ausschalten
		map.removeInteraction(draw);
		
		// das gezeichnete Feature wurde an die Funktion übergeben
		var f = e.feature;
		// die Geometrie aus dem Feature extrahieren, weil die Koordinaten gebraucht werden
		var g = f.getGeometry();
		// in geographische Koordinaten transformieren
		var coord4326 = transform(g.getCoordinates(), 'EPSG:3857', 'EPSG:4326');
		// Koordinaten in das Formular eintragen
		$('#poiLongitude').val(format(coord4326, '{x}', 6));
		$('#poiLatitude').val(format(coord4326, '{y}', 6));
	}, this);
	
	// das Ändern der Lages des Punktes ermöglichen
	modify = new Modify({
		source: pointSource
	});
	map.addInteraction(modify);

	// nach dem Ändern
	modify.on('modifyend', function(e) {
		// alle Features werden übergeben ...
		var features = e.features;
		// ... daher wird nur das erste Feature extrahiert
		var g = features.item(0).getGeometry();
		// in geographische Koordinaten transformieren
		var coord4326 = transform(g.getCoordinates(), 'EPSG:3857', 'EPSG:4326');
		// Koordinaten in das Formular eintragen
		$('#poiLongitude').val(format(coord4326, '{x}', 6));
		$('#poiLatitude').val(format(coord4326, '{y}', 6));
	});
}

// ---------------------------------------------------------------
// Wenn im Stadtrad-Formular auf Speichern geklickt wird.
// ---------------------------------------------------------------

$('#formPOI').submit(function(e) {
	// damit keine neue Seite geladen wird
	e.preventDefault();
	
	// Je nachdem welcher Menüpunkt ausgewählt wurde ...
	var url;
	// ... wird entweder eine neue Stadtrad-Station hinzugefügt oder ...
	if ( $('#poiControlDraw').is(':checked') ) {
		url = 'http://localhost:8081/createPOI';
	// die geänderten Daten einer bereits bestehenden Station gespeichert.
	} else if ( $('#poiControlModify').is(':checked') ) {
		url = 'http://localhost:8081/updatePOI';
	}
	
	// Daten per AJAX (Asynchronous JavaScript and XML) übertragen
	var form_data = $(this).serialize();
	$.ajax({
		type: 'POST',
		url: url,
		data: form_data
	}).done(function(response) {
		// Meldung, dass die Daten gespeichert/geändert wurden, werden vom Server geliefert
		$('#poiMessage').html(response);
		// Meldung nach 5s wieder verschwinden lassen
		setTimeout(removeMessage, 5000);
	}).fail(function(response) {
		$('#poiMessage').html(response);
	});

	// Wenn eine neue Stadtrad-Station erstellt wurde, dann ...
	if ( $('#poiControlDraw').is(':checked') ) {
		// ... wird das Feature dem Stadtrad-Layer hinzugefügt und ...
		var coordinate = [ $('#poiLongitude').val(), $('#poiLatitude').val() ];
		var f = pointSource.getClosestFeatureToCoordinate(coordinate);
		// ... die Properties mit den Werten aus dem Formular befüllt.
		f.setProperties({
			'ID':$('#poiId').val(),
			'CAT':$('#poiCat').val(),
			'NAME':$('#poiName').val(),
			'STREET':$('#poiStreet').val(),
			'ZIP':$('#poiZip').val(),
			'CITY':$('#poiCity').val(),
			'TEL':$('#poiTel').val(),
			'POC':$('#poiPoc').val(),
			'INFO':$('#poiInfo').val()
		});
		// Das neu erzeugte Stadtrad-Feature wird dem Stadtrad-Layer hinzugefügt.
		poi_layer.getSource().addFeature(f);
		// Die Quelle, in der das Stadtrad-Feature erstellt wurde, wird geleert.
		pointSource.clear();
		
		// Stadtrad-Formular verstecken
		$('#formPOI').hide();
	}

	// Wenn die Daten einer Stadtrad-Station geändert wurden, ...
	if ( $('#poiControlModify').is(':checked') ) {
		// ... werden die geänderten Formulardaten in das Feature geschrieben.
		feature.setProperties({
			'CAT':$('#poiCat').val(),
			'NAME':$('#poiName').val(),
			'STREET':$('#poiStreet').val(),
			'ZIP':$('#poiZip').val(),
			'CITY':$('#poiCity').val(),
			'TEL':$('#poiTel').val(),
			'POC':$('#poiPoc').val(),
			'INFO':$('#poiInfo').val()
		});
		
		// Stadtrad-Formular verstecken
		$('#formPOI').hide();
	}

	// alle Interaktionen (snap, draw, modify) werden deaktiviert
	removeInteractions();
	
	// CRUD-Aktionen auf nix setzen
	$('#poiControlNone').prop('checked', true);

	// DISABLE andere Menüpunkte 
	$('#poiControlDraw').attr('disabled', false);
	$('#poiControlModify').attr('disabled', false);
	$('#poiControlDelete').attr('disabled', false);
});


// ---------------------------------------------------------------
// Änderungen aus dem Feature in das Formular schreiben
// ---------------------------------------------------------------

function updatePOI(f) {
	var coordinate = f.getGeometry().getCoordinates();
	var coord4326 = transform(coordinate, 'EPSG:3857', 'EPSG:4326');
	$('#poiLongitude').val(format(coord4326, "{x}", 6));
	$('#poiLatitude').val(format(coord4326, "{y}", 6));
	$('#poiId').val(f.get('ID'));
	$('#poiCat').val(f.get('CAT'));
	$('#poiName').val(f.get('NAME'));
	$('#poiStreet').val(f.get('STREET'));
	$('#poiZip').val(f.get('ZIP'));
	$('#poiCity').val(f.get('CITY'));
	$('#poiTel').val(f.get('TEL'));
	$('#poiPoc').val(f.get('POC'));
	$('#poiInfo').val(f.get('INFO'));

	modify.on('modifyend', function(e) {
		updatePOI(f);
	});
}

// ---------------------------------------------------------------
// Stadtrad-Station löschen
// ---------------------------------------------------------------

// Popup, wenn eine Stadtrad-Station gelöscht werden soll
function deletePOI(f) {
	content.innerHTML = "<p>Kunden-ID: " + f.get("ID") + "</span></p>" +
						"<p>" + f.get("NAME") + "</p>" +
						"<p>" + f.get("STREET") + "</p>" +
						"<p>Soll dieser Punkt gelöscht werden?</p>" +
						"<button type='button' class='btn btn-primary' id='poiDelete'>Löschen</button>&nbsp;" +
						"<button type='button' class='btn btn-primary' id='poiAbbruch'>Abbruch</button>";
	overlay.setPosition(f.getGeometry().getCoordinates());

	// Daten per AJAX (Asynchronous JavaScript and XML) übertragen
	$('#poiDelete').click(function() {
		$.ajax({
			type: 'POST',
			url: 'http://localhost:8081/deletePOI',
			data: "poiId=" + f.get('ID')
		}).done(function(response) {
			$('#poiMessage').html(response);
			setTimeout(removeMessage, 5000);
		}).fail(function(response) {
			$('#poiMessage').html(response);
		});
		poi_layer.getSource().removeFeature(f);
		// Popup schließen
		overlay.setPosition(undefined);
		closer.blur();
	});

	$('#poiAbbruch').click(function() {
		// Popup schließen
		overlay.setPosition(undefined);
		closer.blur();
	});
}

// ---------------------------------------------------------------
// Funktionen, die wiederkehrende Aktionen ausführen
// ---------------------------------------------------------------

// Meldung löschen
function removeMessage() {
	$('#poiMessage').empty();
}

// Interaktionen deaktivieren
function removeInteractions() {
	map.removeInteraction(draw);
	map.removeInteraction(snap);
	map.removeInteraction(modify);
}

// Formularinhalte löschen
function clearForm() {
	$('#poiId').val('');
	$('#poiCat').val('');
	$('#poiName').val('');
	$('#poiStreet').val('');
	$('#poiZip').val('');
	$('#poiCity').val('');
	$('#poiTel').val('');
	$('#poiPoc').val('');
	$('#poiInfo').val('');
}

// ---------------------------------------------------------------
// Popup
// ---------------------------------------------------------------

// Zugriff auf die Popup-Elemente herstellen.
var container = document.getElementById('popup');
var content = document.getElementById('popup-content');
var closer = document.getElementById('popup-closer');

// Ein Overlay erstellen, um das Popup auf der Karte zu verankern.
var overlay = new Overlay({
		element: container,
		autoPan: true,
		autoPanAnimation: {
			duration: 250
		}
});

export { overlay };
//map.addOverlay(overlay);

// Einen click-Handler zufügen, um das Popup zu schließen.
closer.onclick = function() {
	// Popup schließen
	overlay.setPosition(undefined);
	closer.blur();
	
	return false;
};